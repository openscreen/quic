# Copyright (c) 2013 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

if (current_cpu == "arm" || current_cpu == "arm64") {
  import("//build/config/arm.gni")
}

config("zlib_config") {
  include_dirs = [ "." ]
}

config("zlib_internal_config") {
  defines = [ "ZLIB_IMPLEMENTATION" ]
  cflags = [ "-w" ]  # Disable all warnings.
}

use_arm_neon_optimizations = false
if (current_cpu == "arm" || current_cpu == "arm64") {
  if (arm_use_neon) {
    use_arm_neon_optimizations = true
  }
}

use_x86_x64_optimizations = current_cpu == "x86" || current_cpu == "x64"

config("zlib_adler32_simd_config") {
  if (use_x86_x64_optimizations) {
    defines = [ "ADLER32_SIMD_SSSE3" ]
  }

  if (use_arm_neon_optimizations) {
    defines = [ "ADLER32_SIMD_NEON" ]
  }
}

source_set("zlib_adler32_simd") {
  visibility = [ ":*" ]

  if (use_x86_x64_optimizations) {
    sources = [
      "adler32_simd.c",
      "adler32_simd.h",
    ]

    cflags = [ "-mssse3" ]
  }

  if (use_arm_neon_optimizations) {
    sources = [
      "adler32_simd.c",
      "adler32_simd.h",
    ]
  }

  configs += [ ":zlib_internal_config" ]

  public_configs = [ ":zlib_adler32_simd_config" ]
}

# CRC32 is not properly linked on ARM64.
if (use_arm_neon_optimizations && current_cpu == "arm") {
  config("zlib_arm_crc32_config") {
    # Neon is only available on ARM8.
    defines = [ "CRC32_ARMV8_CRC32" ]
    if (is_linux || is_chromeos) {
      defines += [ "ARMV8_OS_LINUX" ]
    } else if (is_fuchsia) {
      defines += [ "ARMV8_OS_FUCHSIA" ]
    } else {
      assert(false, "Unsupported ARM OS")
    }
  }

  source_set("zlib_arm_crc32") {
    visibility = [ ":*" ]

    include_dirs = [ "." ]

    cflags_c = []
    if (!is_clang) {
      cflags_c = [ "-march=armv8-a+crc" ]
    }

    sources = [
      "arm_features.c",
      "arm_features.h",
      "crc32_simd.c",
      "crc32_simd.h",
    ]

    configs += [ ":zlib_internal_config" ]

    public_configs = [ ":zlib_arm_crc32_config" ]
  }
}

config("zlib_inflate_chunk_simd_config") {
  if (use_x86_x64_optimizations) {
    defines = [ "INFLATE_CHUNK_SIMD_SSE2" ]

    if (current_cpu == "x64") {
      defines += [ "INFLATE_CHUNK_READ_64LE" ]
    }
  }

  if (use_arm_neon_optimizations) {
    defines = [ "INFLATE_CHUNK_SIMD_NEON" ]
    if (current_cpu == "arm64") {
      defines += [ "INFLATE_CHUNK_READ_64LE" ]
    }
  }
}

source_set("zlib_inflate_chunk_simd") {
  visibility = [ ":*" ]

  if (use_x86_x64_optimizations || use_arm_neon_optimizations) {
    include_dirs = [ "." ]

    sources = [
      "contrib/optimizations/chunkcopy.h",
      "contrib/optimizations/inffast_chunk.c",
      "contrib/optimizations/inffast_chunk.h",
      "contrib/optimizations/inflate.c",
    ]
  }

  configs += [ ":zlib_internal_config" ]

  public_configs = [ ":zlib_inflate_chunk_simd_config" ]
}

config("zlib_crc32_simd_config") {
  if (use_x86_x64_optimizations) {
    defines = [ "CRC32_SIMD_SSE42_PCLMUL" ]
  }
}

source_set("zlib_crc32_simd") {
  visibility = [ ":*" ]

  if (use_x86_x64_optimizations) {
    sources = [
      "crc32_simd.c",
      "crc32_simd.h",
    ]

    cflags = [
      "-msse4.2",
      "-mpclmul",
    ]
  }

  configs += [ ":zlib_internal_config" ]

  public_configs = [ ":zlib_crc32_simd_config" ]
}

source_set("zlib_x86_simd") {
  visibility = [ ":*" ]

  if (use_x86_x64_optimizations) {
    sources = [
      "crc_folding.c",
      "fill_window_sse.c",
    ]

    cflags = [
      "-msse4.2",
      "-mpclmul",
    ]
  } else {
    sources = [
      "simd_stub.c",
    ]
  }

  configs += [ ":zlib_internal_config" ]
}

static_library("zlib") {
  # Don't stomp on "libzlib"
  output_name = "chrome_zlib"

  sources = [
    "adler32.c",
    "chromeconf.h",
    "compress.c",
    "contrib/optimizations/insert_string.h",
    "crc32.c",
    "crc32.h",
    "deflate.c",
    "deflate.h",
    "gzclose.c",
    "gzguts.h",
    "gzlib.c",
    "gzread.c",
    "gzwrite.c",
    "infback.c",
    "inffast.c",
    "inffast.h",
    "inffixed.h",
    "inflate.h",
    "inftrees.c",
    "inftrees.h",
    "trees.c",
    "trees.h",
    "uncompr.c",
    "x86.h",
    "zconf.h",
    "zlib.h",
    "zutil.c",
    "zutil.h",
  ]

  defines = []
  deps = []

  if (use_x86_x64_optimizations || use_arm_neon_optimizations) {
    deps += [
      ":zlib_adler32_simd",
      ":zlib_inflate_chunk_simd",
    ]

    if (use_x86_x64_optimizations) {
      sources += [ "x86.c" ]
      deps += [ ":zlib_crc32_simd" ]
    } else if (current_cpu == "arm" && use_arm_neon_optimizations) {
      sources += [ "contrib/optimizations/slide_hash_neon.h" ]
      deps += [ ":zlib_arm_crc32" ]
    }
  } else {
    sources += [ "inflate.c" ]
  }

  configs += [ ":zlib_internal_config" ]

  public_configs = [ ":zlib_config" ]

  deps += [ ":zlib_x86_simd" ]
  allow_circular_includes_from = deps
}

config("minizip_warnings") {
  visibility = [ ":*" ]
  cflags = [ "-w" ]  # Disable all warnings.
}

static_library("minizip") {
  sources = [
    "contrib/minizip/ioapi.c",
    "contrib/minizip/ioapi.h",
    "contrib/minizip/unzip.c",
    "contrib/minizip/unzip.h",
    "contrib/minizip/zip.c",
    "contrib/minizip/zip.h",
  ]

  if (is_mac) {
    # Mac, Android and the BSDs don't have fopen64, ftello64, or fseeko64. We
    # use fopen, ftell, and fseek instead on these systems.
    defines = [ "USE_FILE32API" ]
  }

  deps = [
    ":zlib",
  ]

  configs += [ ":minizip_warnings" ]

  # Need access to "third_party/zlib/foo.h"
  include_dirs = [ "../../" ]
  public_configs = [ ":zlib_config" ]
}

executable("zlib_bench") {
  include_dirs = [ "." ]

  sources = [
    "contrib/bench/zlib_bench.cc",
  ]

  deps = [
    ":zlib",
  ]
}
